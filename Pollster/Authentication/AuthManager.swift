//
//  AuthManager.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 22/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation

class AuthManager {
    
    static let shared = AuthManager(email: UserCredentials.email, password: UserCredentials.password)
    
    private let email: String!
    private let password: String!
    
    private var token: String?
    
    private init(email: String, password: String) {
        self.email = email
        self.password = password
    }
    
    /*
     Returns stored token if already present, otherwise requests new token and stores it.
     Handles refreshing of the token.
    */
    public func accessToken(completion: @escaping (_ result: String?) -> Void) {
        if let token = self.token {
            completion(token)
        } else {
            APIRequestAccessToken(grantType: "password", username: email, password: password)
                .dispatch(onSuccess: { (response) in
                    self.token = response.accessToken
                    completion(response.accessToken)
                }) { (response, error) in
                    completion(nil)
                    debugPrint(response.debugDescription)
            }
        }
    }
    
    /*
     If the stored token is invalid, sets token value to nil
     so next time new token will be requested when needed
     */
    public func expireToken() {
        self.token = nil
    }

}
