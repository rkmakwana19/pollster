//
//  Constants.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 22/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation

struct UserCredentials {
    static let email = "carlos@nimbl3.com"
    static let password = "antikera"
}

struct EndPoints {
    static let baseURL = "https://nimble-survey-api.herokuapp.com"
    static let authToken = "/oauth/token"
    static let fetchSurveys = "/surveys.json"
}

struct ImageAssetNames {
    static let bulletSelected = "circleFilled"
    static let bulletDefault = "circle"
}
