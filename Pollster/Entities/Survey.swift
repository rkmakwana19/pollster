//
//  Survey.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 21/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation

struct Survey: Codable {
    
    let id: String?
    let title: String?
    let description: String?
    let background: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case description
        case background = "cover_image_url"
    }
}
