//
//  APIRequest.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 21/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//
// Thanks to Dave Poirier for showing clean and concise way of writing API client.
// Source: https://medium.com/swift2go/minimal-swift-api-client-9ea1c9c7946

import Foundation

protocol APIEndpoint {
    func endpoint() -> String
}

class APIRequest {
    struct ErrorResponse: Codable {
        let status: String
        let code: Int
        let message: String
    }
    
    enum APIError: Error {
        case unauthorised
        case invalidEndpoint
        case errorResponseDetected
        case decodingError
        case noData
    }
    
    // MARK:- Create URL Request
    
    public static func urlRequest(from request: APIEndpoint) -> URLRequest? {
        let endpoint = request.endpoint()
        guard let endpointUrl = URL(string: "\(EndPoints.baseURL)\(endpoint)") else {
            return nil
        }
        
        let endpointRequest = URLRequest(url: endpointUrl)
        return endpointRequest
    }
    
    // MARK:- POST
    
    public static func post<R: Codable & APIEndpoint, T: Codable, E: Codable>(
        request: R,
        onSuccess: @escaping ((_: T) -> Void),
        onError: @escaping ((_: E?, _: Error) -> Void)) {
        
        guard var endpointRequest = self.urlRequest(from: request) else {
            onError(nil, APIError.invalidEndpoint)
            return
        }
        endpointRequest.httpMethod = "POST"
        endpointRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            endpointRequest.httpBody = try JSONEncoder().encode(request)
        } catch {
            onError(nil, error)
            return
        }
        
        URLSession.shared.dataTask(
            with: endpointRequest,
            completionHandler: { (data, urlResponse, error) in
                DispatchQueue.main.async {
                    self.processResponse(data, urlResponse, error, onSuccess: onSuccess, onError: onError)
                }
        }).resume()
    }
    
    // MARK:- GET
    
    public static func get<R: Codable & APIEndpoint, T: Codable, E: Codable>(
        request: R,
        onSuccess: @escaping ((_: T) -> Void),
        onError: @escaping ((_: E?, _: Error) -> Void)) {
        
        guard var endpointRequest = self.urlRequest(from: request) else {
            onError(nil, APIError.invalidEndpoint)
            return
        }
        endpointRequest.httpMethod = "GET"
        
        URLSession.shared.dataTask(
            with: endpointRequest,
            completionHandler: { (data, urlResponse, error) in
                DispatchQueue.main.async {
                    self.processResponse(data, urlResponse, error, onSuccess: onSuccess, onError: onError)
                }
        }).resume()
    }
    
    // MARK:- Process Request
    
    public static func processResponse<T: Codable, E: Codable>(
        _ dataOrNil: Data?,
        _ urlResponseOrNil: URLResponse?,
        _ errorOrNil: Error?,
        onSuccess: ((_: T) -> Void),
        onError: ((_: E?, _: Error) -> Void)) {
        
        if let urlResponse = urlResponseOrNil as? HTTPURLResponse {
            if urlResponse.statusCode == 401 {
                onError(nil, APIError.unauthorised)
            }
        }
        
        if let data = dataOrNil {
            do {
                let decodedResponse = try JSONDecoder().decode(T.self, from: data)
                onSuccess(decodedResponse)
            } catch {
                let originalError = error
                
                do {
                    let errorResponse = try JSONDecoder().decode(E.self, from: data)
                    onError(errorResponse, APIError.decodingError)
                } catch {
                    onError(nil, originalError)
                }
            }
        } else {
            onError(nil, errorOrNil ?? APIError.noData)
        }
    }
}
