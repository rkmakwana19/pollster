//
//  APIFetchSurveysRequest.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 21/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation

struct APIFetchSurveysRequest: APIEndpoint, Codable {
    let page: Int
    let itemCount: Int
    let token: String
    
    private enum CodingKeys: String, CodingKey {
        case page
        case itemCount = "per_page"
        case token = "access_token"
    }
    
    func endpoint() -> String {
        let path = EndPoints.fetchSurveys
        guard let parameters = try? URLQueryEncoder.encode(self) else { fatalError("Wrong parameters") }
        return "\(path)?\(parameters)"
    }
    
    func dispatch(
        onSuccess successHandler: @escaping ((_: [Survey]) -> Void),
        onFailure failureHandler: @escaping ((_: APIRequest.ErrorResponse?, _: Error) -> Void)) {
        
        APIRequest.get(
            request: self,
            onSuccess: successHandler,
            onError: failureHandler)
    }
}

