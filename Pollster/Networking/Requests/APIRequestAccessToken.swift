//
//  APIRequestAccessToken.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 22/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation

struct AccessToken: Codable {
    let accessToken: String
    
    private enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
    }
}

struct APIRequestAccessToken: APIEndpoint, Codable {
    let grantType: String
    let username: String
    let password: String
    
    private enum CodingKeys: String, CodingKey {
        case grantType = "grant_type"
        case username
        case password
    }
    
    func endpoint() -> String {
        return EndPoints.authToken
    }
    
    func dispatch(
        onSuccess successHandler: @escaping ((_: AccessToken) -> Void),
        onFailure failureHandler: @escaping ((_: APIRequest.ErrorResponse?, _: Error) -> Void)) {
        
        APIRequest.post(
            request: self,
            onSuccess: successHandler,
            onError: failureHandler)
    }
}
