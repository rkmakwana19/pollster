//
//  ImageCache.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 23/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

/*
 Uses SDWebImageCache to prefetch image URLs in memory
*/

import Foundation
import SDWebImage

public enum ImageCache {
    
    static func prefetchImage(for urls: [URL]) {
        SDWebImagePrefetcher.shared().prefetchURLs(urls)
    }
    
    static func setImage(on imageView: UIImageView, with url: URL?) {
        guard let url = url else {
            return
        }
        imageView.sd_setImage(with: url, placeholderImage: nil, options: .refreshCached, completed: nil)
    }
    
}
