//
//  SurveyDetailConfigurator.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 24/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation

protocol SurveyDetailConfigurator {
    func configure(detailViewController: SurveyDetailViewController)
}

class SurveyDetailConfiguratorImplementation: SurveyDetailConfigurator {
    let survey: Survey
    
    init(survey: Survey) {
        self.survey = survey
    }
    
    func configure(detailViewController: SurveyDetailViewController) {
        let router = SurveysDetailRouterImplementation(detailViewController: detailViewController)
        
        let presenter = SurveyDetailPresenterImplementation(view: detailViewController,
                                                            survey: survey,
                                                            router: router)
        
        detailViewController.presenter = presenter
    }
}
