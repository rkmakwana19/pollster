//
//  SurveyDetailPresenter.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 24/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation

protocol SurveyDetailPresenter {
    var router: SurveysDetailRouter { get }
    func viewDidLoad()
}

protocol SurveyDetailView: class {
    func display(title: String?)
    func display(background: URL?)
}

class SurveyDetailPresenterImplementation: SurveyDetailPresenter {
    fileprivate weak var view: SurveyDetailView?
    internal let router: SurveysDetailRouter
    
    fileprivate let survey: Survey
    
    init(view: SurveyDetailView,
         survey: Survey,
         router: SurveysDetailRouter) {
        self.view = view
        self.survey = survey
        self.router = router
    }
    
    // MARK:- SurveyDetailPresenter
    
    func viewDidLoad() {
        view?.display(title: survey.title)
        if let imageURL = survey.background {
            let highResURL = URL(string: imageURL.appending("l"))
            view?.display(background: highResURL)
        }
    }
    

}
