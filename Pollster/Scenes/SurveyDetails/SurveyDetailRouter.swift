//
//  SurveyDetailRouter.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 24/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation

protocol SurveysDetailRouter { }

class SurveysDetailRouterImplementation: SurveysDetailRouter {
    fileprivate weak var detailViewController: SurveyDetailViewController?
    fileprivate var survey: Survey!
    
    init(detailViewController: SurveyDetailViewController) {
        self.detailViewController = detailViewController
    }
}
