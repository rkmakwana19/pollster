//
//  SurveyDetailViewController.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 24/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import UIKit

class SurveyDetailViewController: UIViewController, SurveyDetailView {

    var configurator: SurveyDetailConfigurator!
    var presenter: SurveyDetailPresenter!
    
    @IBOutlet weak var imgViewBackground: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configurator.configure(detailViewController: self)
        presenter.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- SurveyDetailView

    func display(title: String?) {
        self.navigationItem.title = title
    }
    
    func display(background: URL?) {
        if let url = background {
            ImageCache.setImage(on: self.imgViewBackground, with: url)
        }
    }
}
