//
//  SurveysCollectionViewCell.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 22/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import UIKit

class SurveysCollectionViewCell: UICollectionViewCell, SurveyCellView {
   
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgViewBackground: UIImageView!
    
    // MARK:- SurveCellView
    
    func display(title: String?) {
        lblTitle.text = title
    }
    
    func display(description: String?) {
        lblDescription.text = description
    }
    
    func display(background: URL?) {
        ImageCache.setImage(on: imgViewBackground, with: background)
    }
}
