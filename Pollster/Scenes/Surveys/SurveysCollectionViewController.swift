//
//  SurveysCollectionViewController.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 20/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import UIKit
import MYTableViewIndex

private let reuseIdentifier = "SurveyCell"

class SurveysCollectionViewController: UICollectionViewController {
    
    var loader: UIActivityIndicatorView!
    var detailsButton: UIButton!
    
    var configurator = SurveysConfiguratorImplementation()
    var presenter: SurveysPresenter!
    
    fileprivate var tableViewIndexController: TableViewIndexController!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loader = createLoader()
        view.addSubview(loader)
        
        detailsButton = createDetailsButton()
        
        configurator.configure(surveysViewController: self)
        presenter.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if tableViewIndexController == nil {
            configureTableViewIndex()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Handle device orientation changes
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        guard let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayout.invalidateLayout()
    }
    
    // MARK:- Button Actions
    
    @objc func detailsButtonAction(_ sender: UIButton) {
        let rect = CGRect(origin: collectionView!.contentOffset, size: collectionView!.bounds.size)
        let point = CGPoint(x: rect.midX, y: rect.midY)
        if let indexPath = collectionView!.indexPathForItem(at: point) {
            presenter.showSurveyDetail(index: indexPath.section)
        }
    }
    
    @IBAction func btnRefresh(_ sender: UIBarButtonItem) {
        presenter.reloadSurveys()
    }

}

// MARK:- UICollectionView

extension SurveysCollectionViewController {
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return presenter.surveysCount
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        /*
         Always return 1 as the data is mapped to the sections,
         for sake of simplicity about using bulleted index
         */
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! SurveysCollectionViewCell
        
        // Configure the cell
        presenter.configure(cell: cell, forIndex: indexPath.section)
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        highlightVisibleIndex(at: indexPath.section)
    }
}

extension SurveysCollectionViewController: UICollectionViewDelegateFlowLayout,
                                           UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.bounds.size.width, height: view.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        let indexes = indexPaths.map( { $0.section } )
        presenter.prefetchImages(for: indexes)
        
        if indexPaths.last?.section == presenter.surveysCount-2 {
            presenter.fetchNextPage()
        }
    }
}

// MARK:- Bulleted Index

extension SurveysCollectionViewController: TableViewIndexDataSource, TableViewIndexDelegate {
    
    func configureTableViewIndex() {
        tableViewIndexController = TableViewIndexController(scrollView: collectionView!)
        tableViewIndexController.tableViewIndex.delegate = self
        tableViewIndexController.tableViewIndex.dataSource = self
    }
    
    func indexItems(for tableViewIndex: TableViewIndex) -> [UIView] {
        var items = [UIView]()
        for _ in 0..<presenter.surveysCount {
            let item = ImageItem(image: UIImage(named: ImageAssetNames.bulletDefault)!)
            item.contentInset = UIEdgeInsets(top: 0, left: 0.5, bottom: -4, right: 0.5)
            item.tintColor = UIColor.white
            items.append(item)
        }
        return items
    }
    
    func tableViewIndex(_ tableViewIndex: TableViewIndex, didSelect item: UIView, at index: Int) -> Bool {
        let originalOffset = collectionView!.contentOffset
        collectionView!.scrollToItem(at: IndexPath(row: 0, section: index), at: .top, animated: false)
        highlightVisibleIndex(at: index)
        return collectionView!.contentOffset != originalOffset
    }

    func highlightVisibleIndex(at index: Int) {
        for (offset, indexItem) in tableViewIndexController.tableViewIndex.items.enumerated() {
            let item = indexItem as! ImageItem
            item.image = (offset == index) ? UIImage(named: ImageAssetNames.bulletSelected) : UIImage(named: ImageAssetNames.bulletDefault)
        }
    }
    
}

// MARK:- SurveysView

extension SurveysCollectionViewController: SurveysView {
    
    func refreshListing() {
        collectionView?.reloadData()
        tableViewIndexController.tableViewIndex.reloadData()
    }
    
    func displayErrorAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default))
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayLoader(show: Bool) {
        if show {
            self.loader.startAnimating()
        }else {
            self.loader.stopAnimating()
        }
    }
    
    func detailsButtonHidden(status: Bool) {
        self.detailsButton.isHidden = status
    }
}

// MARK:- Create UI Components
extension SurveysCollectionViewController {
    
    func createLoader() -> UIActivityIndicatorView {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator.center = view.center
        indicator.hidesWhenStopped = true
        return indicator
        
    }
    
    func createDetailsButton() -> UIButton {
        // Create UIButton with constraints
        let button = UIButton(type: .custom)
        button.setTitle("Take the survey", for: .normal)
        button.titleLabel?.font = UIFont(name: "Trebuchet MS-Bold", size: 16.0)
        button.setTitleColor(UIColor.white, for: .normal)
        button.addTarget(self, action: #selector(detailsButtonAction(_:)), for: .touchUpInside)
        button.backgroundColor = UIColor(red:0.95, green:0.25, blue:0.25, alpha:1.0)
        button.layer.cornerRadius =  24.0
        button.layer.masksToBounds = true
        view.addSubview(button)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor,
                                       constant: -50.0).isActive = true
        button.centerXAnchor.constraint(equalTo: self.view.layoutMarginsGuide.centerXAnchor,
                                        constant: 0).isActive = true
        button.widthAnchor.constraint(equalToConstant: 200.0).isActive = true
        button.heightAnchor.constraint(equalToConstant: 48.0).isActive = true
        return button
    }
}
