//
//  SurveysConfigurator.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 23/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation

protocol SurveysConfigurator {
    func configure(surveysViewController: SurveysCollectionViewController)
}

class SurveysConfiguratorImplementation: SurveysConfigurator {
    
    func configure(surveysViewController: SurveysCollectionViewController) {
        let router = SurveysViewRouterImplementation(surveysViewController: surveysViewController)
        let worker = SurveysWorkerImplementation()
        let presenter = SurveysPresenterImplementation(view: surveysViewController,
                                                       worker: worker,
                                                       router: router)
        
        surveysViewController.presenter = presenter
    }
}
