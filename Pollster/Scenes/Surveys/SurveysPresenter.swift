//
//  SurveysPresenter.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 22/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation

protocol SurveysPresenter {
    var surveysCount: Int { get }
    var router: SurveysViewRouter { get }
    func viewDidLoad()
    func reloadSurveys()
    func fetchNextPage()
    func prefetchImages(for indexes: [Int])
    func configure(cell: SurveyCellView, forIndex index: Int)
    func showSurveyDetail(index: Int)
}

protocol SurveysView: class {
    func refreshListing()
    func displayLoader(show: Bool)
    func detailsButtonHidden(status: Bool)
    func displayErrorAlert(title: String, message: String)
}

protocol SurveyCellView {
    func display(title: String?)
    func display(description: String?)
    func display(background: URL?)
}

class SurveysPresenterImplementation: SurveysPresenter {
    fileprivate weak var view: SurveysView?
    internal let router: SurveysViewRouter
    fileprivate let worker: SurveysWorker
    
    var surveys = [Survey]()
    
    // Number of items for fetching surveys list at once
    fileprivate let fetchCount = 5
    
    init(view: SurveysView,
         worker: SurveysWorker,
         router: SurveysViewRouter) {
        self.view = view
        self.worker = worker
        self.router = router
    }
    
    var surveysCount: Int {
        return surveys.count
    }
    
    // MARK: - BooksPresenter
    
    func viewDidLoad() {
        view?.detailsButtonHidden(status: true)
        view?.displayLoader(show: true)
        reloadSurveys()
    }
    
    func reloadSurveys() {
        view?.displayLoader(show: true)
        fetchSurveys(for: 1)
    }
    
    func fetchSurveys(for page: Int) {
        worker.fetchSurveys(page: page, itemCount: self.fetchCount) { (surveys, error) in
            if let error = error {
                self.handleReceivedError(error)
            } else if let surveys = surveys {
                self.handleReceivedData(surveys: surveys, page: page)
            }
        }
    }
    
    func fetchNextPage() {
        let page = (self.surveys.count / self.fetchCount) + 1
        fetchSurveys(for: page)
    }
    
    func configure(cell: SurveyCellView, forIndex index: Int) {
        let survey = surveys[index]

        cell.display(title: survey.title)
        cell.display(description: survey.description)
        if let imageURL = survey.background {
            let highResURL = URL(string: imageURL.appending("l"))
            cell.display(background: highResURL)
        }
    }
    
    func prefetchImages(for indexes: [Int]) {
        print(indexes)
        var urls = [URL]()
        for index in indexes {
            if let url = self.surveys[index].background {
                if let highResURL = URL(string: url.appending("l")) {
                    urls.append(highResURL)
                }
            }
        }
        DispatchQueue.global(qos: .background).async {
            ImageCache.prefetchImage(for: urls)
        }
    }
    
    func showSurveyDetail(index: Int) {
        let survey = surveys[index]
        router.presentDetailsView(for: survey)
    }
    
    // MARK: - Private
    
    fileprivate func handleReceivedData(surveys: [Survey], page: Int) {
        // If refreshed the list, clear previous data, otherwise add to existing records
        if page == 1 {
            self.surveys.removeAll()
        }
        self.surveys += surveys
        view?.refreshListing()
        view?.displayLoader(show: false)
        view?.detailsButtonHidden(status: false)
    }
    
    fileprivate func handleReceivedError(_ error: Error) {
        guard let apiError = error as? APIRequest.APIError else {
            view?.displayLoader(show: false)
            view?.displayErrorAlert(title: "Error", message: error.localizedDescription)
            return
        }
        if apiError == APIRequest.APIError.unauthorised {
            AuthManager.shared.expireToken()
            self.fetchNextPage()
        } else {
            view?.displayLoader(show: false)
            view?.displayErrorAlert(title: "Error", message: error.localizedDescription)
        }
    }

}
