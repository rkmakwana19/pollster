//
//  SurveysViewRouter.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 23/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import UIKit

protocol SurveysViewRouter {
    func presentDetailsView(for survey: Survey)
}

class SurveysViewRouterImplementation: SurveysViewRouter {
    fileprivate weak var surveysViewController: SurveysCollectionViewController?
    
    init(surveysViewController: SurveysCollectionViewController) {
        self.surveysViewController = surveysViewController
    }
    
    // MARK: - SurveysViewRouter
    
    func presentDetailsView(for survey: Survey) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let detailsController = storyboard.instantiateViewController(withIdentifier: "SurveyDetails") as? SurveyDetailViewController {
            detailsController.configurator = SurveyDetailConfiguratorImplementation(survey: survey)
            self.surveysViewController?.show(detailsController, sender: nil)
        }
    }
    
}
