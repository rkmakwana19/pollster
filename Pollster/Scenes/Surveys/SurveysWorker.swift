//
//  SurveysWorker.swift
//  Pollster
//
//  Created by Rashmikant Makwana on 24/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation

typealias FetchSurveysCompletionHandler = (_ surveys: [Survey]?, _ error: Error?) -> Void

protocol SurveysWorker {
    func fetchSurveys(page: Int, itemCount: Int, completionHandler: @escaping FetchSurveysCompletionHandler)
}

class SurveysWorkerImplementation: SurveysWorker {
    
    func fetchSurveys(page: Int, itemCount: Int, completionHandler: @escaping FetchSurveysCompletionHandler) {
        DispatchQueue.global(qos: .userInitiated).async {
            AuthManager.shared.accessToken { token in
                guard let token = token else {
                    let authError = NSError(domain: "Auth", code: 0, userInfo: [:])
                    DispatchQueue.main.async {
                        completionHandler(nil, authError)
                    }
                    return
                }
                APIFetchSurveysRequest(page: page, itemCount: itemCount, token: token)
                    .dispatch(
                        onSuccess: { surveys in
                            DispatchQueue.main.async {
                                completionHandler(surveys, nil)
                            }
                    },
                        onFailure: { (errorResponse, error) in
                            DispatchQueue.main.async {
                                completionHandler(nil, error)
                            }
                    })
            }
        }
    }

}
