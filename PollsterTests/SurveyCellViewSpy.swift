//
//  SurveyCellViewSpy.swift
//  PollsterTests
//
//  Created by Rashmikant Makwana on 24/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation
@testable import Pollster

class SurveyCellViewSpy: SurveyCellView {
    var displayedTitle: String?
    var displayedDescription: String?
    var displayedBackround: URL?
    
    func display(title: String?) {
        displayedTitle = title
    }
    
    func display(description: String?) {
        displayedDescription = description
    }
    
    func display(background: URL?) {
        displayedBackround = background
    }
}
