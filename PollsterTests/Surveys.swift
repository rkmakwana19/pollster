//
//  Surveys.swift
//  PollsterTests
//
//  Created by Rashmikant Makwana on 24/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation
@testable import Pollster

extension Survey {
    
    static func createSurveyListing(count: Int) -> [Survey] {
        var items = [Survey]()
        var i = 1
        while (i <= count) {
            let item = Survey(id: String(i),
                              title: "Survey Title \(i)",
                              description: "Survey Description \(i)",
                              background: "Background image url \(i)")
            items.append(item)
            i += 1
        }
        return items
    }
    
}

extension Survey: Equatable {
    public static func ==(lhs: Survey, rhs: Survey) -> Bool {
        return (lhs.id == rhs.id)
    }
}

