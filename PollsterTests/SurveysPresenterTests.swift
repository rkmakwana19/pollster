//
//  SurveysPresenterTests.swift
//  PollsterTests
//
//  Created by Rashmikant Makwana on 24/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import XCTest
@testable import Pollster

class SurveysPresenterTests: XCTestCase {
    let surveysViewRouterSpy = SurveysViewRouterSpy()
    let surveysViewSpy = SurveysViewSpy()
    let surveysWorkerSpy = SurveysWorkerSpy()
    var surveysPresenter: SurveysPresenterImplementation!
    
    override func setUp() {
        super.setUp()
        
        surveysPresenter = SurveysPresenterImplementation(view: surveysViewSpy,
                                                          worker: surveysWorkerSpy,
                                                          router: surveysViewRouterSpy)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSurveysFetchedAndListRefreshedWhenViewLoaded() {
        // Given
        let itemsToBeReturned = Survey.createSurveyListing(count: 2)
        surveysWorkerSpy.fetchItemsToBeReturned = itemsToBeReturned
        
        // When
        surveysPresenter?.viewDidLoad()
        
        // Then
        XCTAssertTrue(surveysViewSpy.refreshListCalled, "List was not refreshed")
    }
    
    func testShowsLoaderWhenAppLaunched() {
        // when
        surveysPresenter.viewDidLoad()
        
        // Then
        XCTAssertTrue(surveysViewSpy.loaderDisplayed)
    }
    
    func testDetailsButtonHiddenWhenBeforeListFetched() {
        // When
        surveysPresenter.viewDidLoad()
        
        // Then
        XCTAssertTrue(surveysViewSpy.detailsButtonHidden, "Details action button should be hidden.")
    }
    
    func testListShowsCorrectNumberOfItems() {
        // Given
        let itemsToBeReturned = Survey.createSurveyListing(count: 6)
        surveysWorkerSpy.fetchItemsToBeReturned = itemsToBeReturned
        
        // When
        surveysPresenter?.viewDidLoad()
        
        // Then
        XCTAssertEqual(surveysPresenter.surveysCount, 6, "List shows incorrect number of items")
    }
    
    func testDetailsButtonNavigatesToNextScreen() {
        // Given
        let items = Survey.createSurveyListing(count: 5)
        let itemToSelect = 1
        surveysPresenter?.surveys = items
        
        // When
        surveysPresenter?.showSurveyDetail(index: itemToSelect)
        
        // Then
        XCTAssertEqual(items[itemToSelect], surveysViewRouterSpy.passedSurvey, "Passed item does not match with the expected item")
    }
    
}
