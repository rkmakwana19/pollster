//
//  SurveysRouterSpy.swift
//  PollsterTests
//
//  Created by Rashmikant Makwana on 24/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation
@testable import Pollster

class SurveysViewRouterSpy: SurveysViewRouter {
    var passedSurvey: Survey?
    
    func presentDetailsView(for survey: Survey) {
        passedSurvey = survey
    }
}
