//
//  SurveysViewSpy.swift
//  PollsterTests
//
//  Created by Rashmikant Makwana on 24/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation
@testable import Pollster

class SurveysViewSpy: SurveysView {
    var refreshListCalled = false
    var loaderDisplayed = false
    var detailsButtonHidden = false
    
    func refreshListing() {
        refreshListCalled = true
    }
    
    func displayLoader(show: Bool) {
        loaderDisplayed = show
    }
    
    func detailsButtonHidden(status: Bool) {
        detailsButtonHidden = status
    }
    
    func displayErrorAlert(title: String, message: String) { }
}
