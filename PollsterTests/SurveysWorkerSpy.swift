//
//  SurveysWorkerSpy.swift
//  PollsterTests
//
//  Created by Rashmikant Makwana on 24/05/19.
//  Copyright © 2019 Rashmikant Makwana. All rights reserved.
//

import Foundation
import UIKit
@testable import Pollster

class SurveysWorkerSpy: SurveysWorker {
    var fetchItemsToBeReturned: [Survey]!
    
    func fetchSurveys(page: Int, itemCount: Int, completionHandler: @escaping FetchSurveysCompletionHandler) {
        completionHandler(fetchItemsToBeReturned, nil)
    }
}
